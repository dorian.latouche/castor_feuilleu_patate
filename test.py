from mud import *

def test():
	b = Box()
	c = Box()
	c.close()
	b.add("castor")
	assert b.affiche()=="Votre boite contient: castor"
	assert c.affiche()=="Boite fermée"

	assert b.is_open()==True	

	a=objet()
	assert a.get_volume()==None
	a.set_volume(10)
	assert a.get_volume()==10

def objet():
	a=Objet(5)
	assert a.get_volume() == 5
	a.set_volume(10)
	assert a.get_volume() == 10

def coucou():
	print("Coucou!")