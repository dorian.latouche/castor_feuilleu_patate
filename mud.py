class Box(object):
	def __init__(self):
		self._contains=[]
		self._open=True
		self._place=None

	def add(self,objet):
		self._contains.append(objet)


	def objet(self):
		return self._contains

	def affiche(self):
		res="Votre boite contient: "
		if self.is_open():
			return res + " ,".join(self._contains)
		else:
			return "Boite fermée" 


	def __contains__(self, objet):
		return objet in self._contains

	def remove(self, objet):
		if self.__contains__(objet):
			self._contains.remove(objet)
			return True
		else:
			return False

	def is_open(self):
		return self._open

	def close(self):
		self._open=False

	def open(self):
		self._open=True


class Objet(object):
	def __init__(self,poids):
		self.volume=poids

	def set_volume(self, volume):
		self.volume=volume

	def get_volume(self):
		return self.volume

